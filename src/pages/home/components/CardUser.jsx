import React from "react";

export default function CardUser({ user }) {
  const formatDate = (date) => {
    const options = {
      year: "numeric",
      month: "long",
      day: "numeric",
      minutes: "numeric",
      hour: "numeric",
    };
    return new Date(date).toLocaleDateString(undefined, options);
  };
  return (
    <React.Fragment>
      {user?.map((data) => (
        <div key={data.id} className="flex justify-center mt-12">
          <div className="card w-full md:w-9/12 bg-base-100 shadow-xl">
            <div className="card-body">
              <div className="flex items-center gap-4">
                <div className="avatar">
                  <div className="w-24 rounded">
                    <img src={data.owner.avatar_url} />
                  </div>
                </div>
                <div>
                  <h2 className="card-title">{data.full_name}</h2>
                  <p>ID : {data.id}</p>
                  <p>Language : {data.language}</p>
                </div>
              </div>
              <div className="card-actions justify-end">
                <label
                  htmlFor={data.id}
                  className="btn bg-rose-500 border-none modal-button"
                >
                  View Details
                </label>
              </div>
            </div>
          </div>

          <input type="checkbox" id={data.id} className="modal-toggle" />
          <div className="modal">
            <div className="modal-box relative w-full">
              <label
                htmlFor={data.id}
                className="btn btn-sm btn-circle absolute right-2 top-2"
              >
                ✕
              </label>
              <div className="card-body">
                <div className="avatar">
                  <div className="w-24 rounded mx-auto my-10">
                    <img src={data.owner.avatar_url} />
                  </div>
                </div>

                <div>
                  <table className="w-full">
                    <tbody>
                      <tr>
                        <div className="flex items-baseline">
                          <td className="font-bold">Repository</td>
                        </div>
                        <td>: {data.name}</td>
                      </tr>
                      <tr>
                        <td className="font-bold">ID</td>
                        <td>: {data.id}</td>
                      </tr>
                      <tr>
                        <td className="font-bold">Language</td>
                        <td>: {data.language}</td>
                      </tr>
                      <tr>
                        <div className="flex items-baseline">
                          <td className="font-bold">Description</td>
                        </div>
                        <td>: {data.description}</td>
                      </tr>
                      <tr>
                        <td className="font-bold">Created At</td>
                        <td>: {formatDate(data.created_at)}</td>
                      </tr>
                      <tr>
                        <td className="font-bold">Updated At</td>
                        <td>: {formatDate(data.updated_at)}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      ))}
    </React.Fragment>
  );
}
