import React from "react";

export default function ListSuggestion({
  suggestion,
  setQuery,
  setSuggestion,
}) {
  const handleClick = (params) => {
    setQuery(params);
    setSuggestion([]);
  };
  return (
    <div className="w-full md:w-1/2 mx-auto">
      <ul className="menu w-4/5 mx-auto mt-5 text-black p-2 rounded-box border">
        {suggestion?.map((data) => (
          <li key={data.id}>
            <a
              className="cursor-pointer px-4"
              onClick={() => handleClick(data.name)}
            >
              {data.name}
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
}
