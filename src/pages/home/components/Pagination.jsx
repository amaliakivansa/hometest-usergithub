import React from "react";

export default function Pagination({ page, prevPage, nextPage }) {
  return (
    <div className="btn-group">
      <button className="btn" onClick={prevPage}>
        {page}
      </button>
      <button className="btn" onClick={nextPage}>
        {page + 1}
      </button>
    </div>
  );
}
