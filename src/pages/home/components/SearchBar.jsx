import React, { useState, useEffect, useCallback } from "react";
import { searchUser, searchUserRepo } from "../../../service/userSearch";
import CardUser from "./CardUser";
import ListSuggestion from "./ListSuggestion";
import Pagination from "./Pagination";
import Select from "./Select";
import debounce from "lodash.debounce";
import { toast } from "react-toastify";

export default function SearchBar() {
  const [query, setQuery] = useState("");
  const [usersResult, setUsersResult] = useState([]);
  const [page, setPage] = useState(1);
  const [pageLimit, setPageLimit] = useState(5);
  const [suggestion, setSuggestion] = useState([]);

  const fetchUserResult = async () => {
    await searchUserRepo(query, page, pageLimit)
      .then((res) => {
        if (res.status === 200) {
          setUsersResult(res.data);
        } else {
          toast.error(res.message);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const debounceCallback = useCallback(
    debounce((m) => setQuery(m), 500),
    []
  );

  const handleChange = (e) => {
    debounceCallback(e.target.value);
    let array = [];
    if (query) {
      searchUser(query, 1, 5).then((res) => {
        if (res.status === 200) {
          array = res.data?.items?.filter((user) => {
            const regex = new RegExp(`${query}`, "gi");
            return user.name.match(regex);
          });
          setSuggestion(array);
        }
      });
    }

    setSuggestion([]);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (query) {
      fetchUserResult();
    }
    setSuggestion([]);
  };

  // Pagination & Select Number of Pagination
  const handlePrevPage = () => {
    setPage((page) => {
      if (page === 1) return page;
      else return page - 1;
    });
  };

  const handleNextPage = () => {
    setPage((page) => page + 1);
  };

  const handlePageLimit = (e) => {
    setPageLimit(parseInt(e.target.value));
  };

  useEffect(() => {
    const displayRepoChange = async () => {
      if (query) {
        await fetchUserResult();
      }
    };
    displayRepoChange();
  }, [page, pageLimit]);

  const handleDelete = () => {
    setQuery("");
    setSuggestion([]);
    setUsersResult([]);
  };

  return (
    <div className="w-full md:w-4/5 mx-auto my-10">
      <div className="flex items-center justify-center gap-2 sm:w-4/12 w-4/5 m-auto">
        <div className="form-control">
          <div className="input-group">
            <input
              type="text"
              placeholder="Search"
              className="input input-bordered"
              value={query}
              onChange={handleChange}
            />
            <button className="btn btn-square" onClick={handleDelete}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-x-lg"
                viewBox="0 0 16 16"
              >
                <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z" />
              </svg>
            </button>
          </div>
        </div>
        <a className="btn btn-primary" onClick={handleSubmit}>
          Search
        </a>
      </div>
      {suggestion?.length !== 0 && (
        <ListSuggestion
          suggestion={suggestion}
          setSuggestion={setSuggestion}
          setQuery={setQuery}
        />
      )}
      <div className="flex justify-between w-full md:w-3/4 mx-auto mt-10">
        <Select pageLimit={handlePageLimit} />
        <div>
          <Pagination
            page={page}
            prevPage={handlePrevPage}
            nextPage={handleNextPage}
          />
        </div>
      </div>

      <CardUser user={usersResult} />
    </div>
  );
}
