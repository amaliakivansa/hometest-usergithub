import React, { useState } from "react";

export default function Select({ pageLimit }) {
  return (
    <select
      className="select select-primary w-24 max-w-xs"
      onChange={pageLimit}
    >
      <option>5</option>
      <option>10</option>
      <option>20</option>
    </select>
  );
}
