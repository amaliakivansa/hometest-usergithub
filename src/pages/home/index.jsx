import React from "react";
import GithubLogo from "../../assets/github.png";
import SearchBar from "./components/SearchBar";

export default function Home() {
  return (
    <div className="w-4/5 mx-auto mt-20">
      <div className="flex items-center justify-center mb-11">
        <img src={GithubLogo} alt="Github Logo" className="w-16" />
        <a className="btn btn-ghost normal-case text-xl">
          Github Search Users' Repository
        </a>
      </div>
      <SearchBar />
    </div>
  );
}
