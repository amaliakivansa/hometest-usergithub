import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import Home from "./home";

export default function Main() {
  return (
    <Switch>
      <Route exact path="/home" component={Home} />
      <Route path="*" render={() => <Redirect to="/home" />} />
    </Switch>
  );
}
