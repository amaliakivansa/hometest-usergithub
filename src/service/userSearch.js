import axios from "axios";
const baseUrl = "https://api.github.com";

export const searchUser = async (query, page, per_page) => {
  try {
    const result = await axios.get(
      `${baseUrl}/search/repositories?q=` + query,
      { params: { page, per_page } }
    );
    return result;
  } catch (error) {
    return error?.response?.data;
  }
};
export const searchUserRepo = async (query, page, per_page) => {
  try {
    const result = await axios.get(`${baseUrl}/users/` + query + "/repos", {
      params: { page, per_page },
    });
    return result;
  } catch (error) {
    return error?.response?.data;
  }
};
